<!DOCTYPE HTML>
<html <?php language_attributes(); ?>>
	<head>
		<title><?php bloginfo( 'name' ); ?></title>
		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="profile" href="http://gmpg.org/xfn/11">
		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
		<!--[if lte IE 8]><script src="<?php bloginfo('template_directory');  ?>/js/ie/html5shiv.js"></script><![endif]-->
		<!--[if lte IE 9]><link rel="stylesheet" href="<?php echo esc_url( get_template_directory_uri() );   ?>/inc/css/ie9.css" /><![endif]-->
		<!--[if lte IE 8]><link rel="stylesheet" href="<?php echo esc_url( get_template_directory_uri() );  ?>/inc/css/ie8.css" /><![endif]-->
		<?php wp_head(); ?>
		<style type="text/css">
			html{
				margin: 0 !important;	
			}
		</style>
	</head>
	<body class="landing">
		<div id="page-wrapper">

			<!-- Header -->
				<header id="header">
					<h1 id="logo"><a href="index.html">Rodolfo Barros</a></h1>
					<nav id="nav">
						<ul>
							<li><a href="#banner" class="scrolly">Inicio</a></li>
							<li><a href="#one" class="scrolly">Quem Sou</a></li>
							<li><a href="#two" class="scrolly">Proposta</a>
								<ul>
									<li><a href="#two" class="scrolly">Segurança</a></li>
									<li><a href="#three" class="scrolly">Saúde</a></li>
									<li><a href="#four" class="scrolly">Educação</a></li>
								</ul>
							</li>
							<li><a href="#six" class="scrolly">Contato</a></li>
							<li><a href="<?php bloginfo('url'); ?>/index.php/blog" class="button">Blog</a></li>
						</ul>
					</nav>
				</header>

			<!-- Banner -->
				<section id="banner">
					<div class="content">
						<header>
							<h1 class="content__heading active">É possível mudar</h1>
							<h1 class="content__heading">Sim, nós podemos</h1>
							<h1 class="content__heading">Participe deste movimento</h1>
							<h1 class="content__heading">#VamosMudarMaceió</h1>
							<h4>Só uma juventude forte e determinada pode mudar a situação da nossa cidade. Faça parte da mudança!</h4>
						</header>
						<!-- <span class="image"><img src="images/pic01.jpg" alt="" /></span> -->
					</div>
					<a href="#one" class="goto-next scrolly">Next</a>
				</section>

			<!-- One -->
				<section id="one" class="spotlight style1 bottom about-video">
					<span class="image fit main"><img src="<?php bloginfo('template_directory'); ?>/inc/img/back1.jpg" alt="" /></span>
					<div class="content">
						<div class="container">
							<div class="row">
								<div class="12u 12u$(medium)">
									<header>
										<h2>Maceió precisa da Juventude</h2>
										<p>Só uma juventude forte e determinada pode mudar a situação da nossa cidade. Faça parte da mudança!
											 Só uma juventude forte e determinada pode mudar a situação da nossa cidade. Faça parte da mudança!
											Só uma juventude forte e determinada pode mudar a situação da nossa cidade. Faça parte da mudança!</p>
									</header>
								</div>
							</div>
							<div class="row">
								<div class="12u 12u$(medium)" id="video-presentation">
									<iframe src="https://player.vimeo.com/video/173078979" width="640" height="360" frameborder="0"
									webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
								</div>
							</div>
						</div>
					</div>
					<a href="#two" class="goto-next scrolly">Next</a>
				</section>

			<!-- Two -->
				<section id="two" class="spotlight style3 right">
					<span class="image fit main"><img src="<?php bloginfo('template_directory'); ?>/inc/img/back3.jpg" alt="" /></span>
					<div class="content">
						<header>
							<h2>EDUCAÇÃO</h2>
						</header>
						<p>São muitos os problemas que estão presentes na educação de nosso município, especialmente na educação pública. São diversos os fatores que proporcionam resultados negativos, um exemplo disso são as crianças que não dominam habilidade de ler e escrever.
Esse fato é resultado direto do que acontece na estrutura educacional de nossa cidade, pois praticamente todos os que atuam na educação recebem baixos salários, professores frustrados que não exercem com profissionalismo ou também esbarram nas dificuldades diárias da realidade escolar, além dos pais que não participam na educação dos filhos, entre muitos outros agravantes.
Este movimento tem como principal bandeira para o desenvolvimento de Maceió, a EDUCAÇÃO. Acreditamos que a valorização do profissional formador de todos os demais é, sobretudo, a mais urgente necessidade de mudança. Professores e alunos são os protagonistas para o desenvolvimento de um povo. Sonhamos por acreditar que juntos alcançaremos os resultados esperados.</p>
						<ul class="actions">
							<li><a href="#" class="button">Saiba Mais</a></li>
						</ul>
					</div>
					<a href="#three" class="goto-next scrolly">Next</a>
				</section>

			<!-- Three -->
				<section id="three" class="spotlight style3 left">
					<span class="image fit main bottom"><img src="<?php bloginfo('template_directory'); ?>/inc/img/back3.jpg" alt="" /></span>
					<div class="content">
						<header>
							<h2>SAÚDE</h2>
						</header>
						<p>O Ministério da Saúde classificou Maceió como um dos municípios com os piores índices de saúde pública do Brasil, a população sofre com dificuldade no atendimento para procedimentos básicos. Entretanto, com a reorganização do orçamento municipal podemos investir em Unidades de Pronto Atendimento em toda a cidade, resultando assim, em uma eficiência maior no atendimento médico e na qualidade de vida do cidadão.</p>
						<ul class="actions">
							<li><a href="#" class="button">Ver mais sobre Saúde</a></li>
						</ul>
					</div>
					<a href="#four" class="goto-next scrolly">Next</a>
				</section>

				<!-- Four -->
					<section id="four" class="spotlight style3 right">
						<span class="image fit main"><img src="<?php bloginfo('template_directory'); ?>/inc/img/back3.jpg" alt="" /></span>
						<div class="content">
							<header>
								<h2>JUVENTUDE</h2>
							</header>
							<p>Ao longo da história a juventude atuou como o motor das grandes transformações sociais, no entanto, é notória a ausência dos jovens na construção das políticas públicas necessárias para o desenvolvimento. Em meio a esse cenário de caos, violência e sonhos frustrados, um grupo de jovens se levanta para mudar Maceió. Liderados por Rodolfo Barros tem como bandeira a indignação com o cenário político e a vontade de lutar por uma nova Maceió. .</p>
							<ul class="actions">
								<li><a href="#" class="button">Ver mais sobre Juventude</a></li>
							</ul>
						</div>
						<a href="#five" class="goto-next scrolly">Next</a>
					</section>

					<!-- Five -->
					<section id="five" class="spotlight style3 left">
						<span class="image fit main bottom"><img src="<?php bloginfo('template_directory'); ?>/inc/img/back3.jpg" alt="" /></span>
						<div class="content">
							<header>
								<h2>MOBILIDADE URBANA</h2>
							</header>
							<p>Nos últimos anos, o debate sobre a mobilidade urbana em Maceió vem se acirrando cada vez mais, haja vista que a cidade vem encontrando dificuldades em desenvolver meios para diminuir a quantidade de congestionamentos ao longo do dia e o excesso de pedestres em áreas centrais dos espaços urbanos. 
Entre as principais soluções para o problema da mobilidade urbana, na visão de muitos especialistas, seria o estímulo aos transportes coletivos públicos, através da melhoria de suas qualidades e eficiências e do desenvolvimento de um trânsito focado na circulação desses veículos. Além disso, o incentivo à utilização de bicicletas, principalmente com a construção de ciclovias e ciclofaixas, também pode ser uma saída a ser mais bem trabalhada.</p>
							<ul class="actions">
								<li><a href="#" class="button">Ver mais sobre Saúde</a></li>
							</ul>
						</div>
						<a href="#six" class="goto-next scrolly">Next</a>
					</section>

						<!-- Six -->
					<section id="six" class="spotlight style3 right">
						<span class="image fit main"><img src="<?php bloginfo('template_directory'); ?>/inc/img/back3.jpg" alt="" /></span>
						<div class="content">
							<header>
								<h2>COMBATE À CORRUPÇÃO</h2>
							</header>
							<p>Todos os anos o Brasil perde 200 bilhões em recursos públicos desviados. Dinheiro que poderia triplicar o orçamento da saúde e educação, aumentar em 30% o investimento em ciência e tecnologia, além de construir 20 milhões de casas populares.  Propomos uma campanha baseada na fiscalização dos gastos públicos com a intenção de empregar esses recursos na área social. Temos como bandeira "as 10 medidas contra a corrupção" propostas pelo Ministério Público Federal.</p>
							<ul class="actions">
								<li><a href="#" class="button">Ver mais sobre Juventude</a></li>
							</ul>
						</div>
						<a href="#seven" class="goto-next scrolly">Next</a>
					</section>

						<!-- Seven -->
					<section id="seven" class="spotlight style3 left">
						<span class="image fit main bottom"><img src="<?php bloginfo('template_directory'); ?>/inc/img/back3.jpg" alt="" /></span>
						<div class="content">
							<header>
								<h2>INCLUSÃO SOCIAL, ESPORTE E LAZER</h2>
							</header>
							<p>A prática do esporte pode transformar as vidas de muitas crianças e adolescentes, estimulando a superação de barreiras e limitações e o crescimento das noções de solidariedade e respeito às diferenças. Quem pratica esportes tem a oportunidade de se tornar um cidadão melhor, porque treina também para a vida, para exercer os seus direitos e compreender os seus deveres com disciplina e determinação.
Também pensando nas regiões em situações de maior vulnerabilidade social, identificamos a necessidade de aliar o esporte ao lazer como instrumentos de inclusão social. O movimento #VamosMudarMaceió tem desenvolvido estratégias junto à sociedade com o objetivo de desenvolver ideias e ações programáticas que valorizem as comunidades de todas as partes da cidade.</p>
							<ul class="actions">
								<li><a href="#" class="button">Ver mais sobre Saúde</a></li>
							</ul>
						</div>
						<a href="#eight" class="goto-next scrolly">Next</a>
					</section>

								<!-- Eight -->
					<section id="eight" class="spotlight style3 right">
						<span class="image fit main"><img src="<?php bloginfo('template_directory'); ?>/inc/img/back3.jpg" alt="" /></span>
						<div class="content">
							<header>
								<h2>EM DEFESA DA VIDA E DA FAMÍLIA</h2>
							</header>
							<p>Por ter convicção de que a família é a mais sólida base de uma sociedade construtiva e em constate desenvolvimento, defendemos a sua origem, bem como, suas conquistas. Diante de tantos abusos patrocinados por governos corruptos e alienados e por uma mídia controladora, lutaremos insensatamente para que a vida seja respeitada e para que o núcleo familiar seja preservado.</p>
							<ul class="actions">
								<li><a href="#" class="button">Ver mais sobre Juventude</a></li>
							</ul>
						</div>
						<a href="#nine" class="goto-next scrolly">Next</a>
					</section>

			<!-- Nine -->
				<section id="nine" class="wrapper style1 special fade-up">
					<div class="container">
						<header class="major">
							<h2>Saiba um pouco sobre mim</h2>
							<p>Iaculis ac volutpat vis non enim gravida nisi faucibus posuere arcu consequat</p>
						</header>
						<div class="box alt">
							<div class="row uniform">
								<section class="4u 6u(medium) 12u$(xsmall)">
									<span class="icon alt major fa-area-chart"></span>
									<h3>Ipsum sed commodo</h3>
									<p>Feugiat accumsan lorem eu ac lorem amet accumsan donec. Blandit orci porttitor.</p>
								</section>
								<section class="4u 6u$(medium) 12u$(xsmall)">
									<span class="icon alt major fa-comment"></span>
									<h3>Eleifend lorem ornare</h3>
									<p>Feugiat accumsan lorem eu ac lorem amet accumsan donec. Blandit orci porttitor.</p>
								</section>
								<section class="4u$ 6u(medium) 12u$(xsmall)">
									<span class="icon alt major fa-flask"></span>
									<h3>Cubilia cep lobortis</h3>
									<p>Feugiat accumsan lorem eu ac lorem amet accumsan donec. Blandit orci porttitor.</p>
								</section>
								<section class="4u 6u$(medium) 12u$(xsmall)">
									<span class="icon alt major fa-paper-plane"></span>
									<h3>Non semper interdum</h3>
									<p>Feugiat accumsan lorem eu ac lorem amet accumsan donec. Blandit orci porttitor.</p>
								</section>
								<section class="4u 6u(medium) 12u$(xsmall)">
									<span class="icon alt major fa-file"></span>
									<h3>Odio laoreet accumsan</h3>
									<p>Feugiat accumsan lorem eu ac lorem amet accumsan donec. Blandit orci porttitor.</p>
								</section>
								<section class="4u$ 6u$(medium) 12u$(xsmall)">
									<span class="icon alt major fa-lock"></span>
									<h3>Massa arcu accumsan</h3>
									<p>Feugiat accumsan lorem eu ac lorem amet accumsan donec. Blandit orci porttitor.</p>
								</section>
							</div>
						</div>
						<footer class="major">
							<ul class="actions">
								<li><a href="#" class="button">Magna sed feugiat</a></li>
							</ul>
						</footer>
					</div>
				</section>

			<!-- Ten -->
				<section id="ten" class="wrapper style2 special fade">
					<div class="container">
						<header>
							<h2>Fale Conosco!</h2>
							<p>Tem alguma ideia ou sugestão?</p>
						</header>
						<div class="container 50%">
						<?php echo do_shortcode('[contact-form-7 id="33" title="Sem nome"]') ?>
						</div>
					</div>
				</section>
				<!-- <?php /*echo do_shortcode('[contact-form-7 id="22" title="Formulário de contato 1"]'); */?> -->

			<!-- Footer -->
				<footer id="footer">
					<div class="row">
						<h3 class="12u">Siga-nos nas redes sociais</h3>
						<div class="12u">
						<ul class="icons">
							<li><a href="#" class="icon alt fa-youtube"><span class="label">YouTube</span></a></li>
							<li><a href="#" class="icon alt fa-facebook"><span class="label">Facebook</span></a></li>
							<!-- <li><a href="#" class="icon alt fa-linkedin"><span class="label">LinkedIn</span></a></li> -->
							<li><a href="#" class="icon alt fa-instagram"><span class="label">Instagram</span></a></li>
							<li><a href="#" class="icon alt fa-snapchat"><span class="label">SnapChat</span></a></li>
							<!-- <li><a href="#" class="icon alt fa-github"><span class="label">GitHub</span></a></li> -->
							<!-- <li><a href="#" class="icon alt fa-envelope"><span class="label">Email</span></a></li> -->
						</ul>
					</div>
					</div>
					<div class="row">
						<div class="12u 12u$(small)"><p>Design por: <a href="http://html5up.net">HTML5 UP</a></p></div>
<!-- 						<div class="12u 12u$(small)"><p>Desenvolvimento: <a>Alex Batista</a></p></div>
 -->						<div class="12u 12u$(small)"><p>&copy; Rodolfo Barros. Todos os direitos reservados</p></div>
					</div>
				</footer>

		</div>

		<!-- Scripts -->

		<!--[if lte IE 8]><script src="<?php bloginfo('template_directory');  ?>/js/ie/respond.min.js"></script><![endif]-->
		<?php wp_footer(); ?>

	</body>
</html>
