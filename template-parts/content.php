<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Shapely
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class('post-snippet mb64'.( is_single() ? ' content': " ")); ?>>
	<header class="entry-header nolist">
		<?php
        
        if(has_post_thumbnail() && !is_single()){
        ?>
         <a class="text-center" href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php
            the_post_thumbnail('shapely-featured',array( 'class' => 'mb24')); ?>
        </a><?php
        }

        if ( is_single() ) {
            setPostViews(get_the_ID());
            if(has_post_thumbnail()):
                ?><div class="post-thumbnail"><?php
                the_post_thumbnail('shapely-featured');
                ?> </div><?php
            endif;
            the_title( '<h1 class="post-title entry-title">', '</h1>' );
            shapely_posted_on();
        } else {
            ?> <div class="title-line">
            <a class="hidden-xs hidden-sm" href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ), get_the_author_meta( 'user_nicename' ) ); ?>"><div class="user-avatar"><?php echo get_avatar( get_the_author_meta( 'ID' ), 100 ); ?></div></a>
            <div class="title-content">
            <?php
            the_title( '<h2 class="post-title entry-title hidden-xs hidden-sm"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>');
            the_title( '<h4 class="post-title entry-title visible-sm-block"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h4>');
            the_title( '<h6 class="post-title entry-title visible-xs-block"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h6>');
            shapely_posted_on();
            ?></div>/</div><?php
        }

		 ?>
		
	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php
            if( !is_single() ){
                // the_excerpt();
            }
            else{
                the_content( sprintf(
                    /* translators: %s: Name of current post. */
                    wp_kses( __( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'shapely' ), array( 'span' => array( 'class' => array() ) ) ),
                    the_title( '<span class="screen-reader-text">"', '"</span>', false )
                ) );
                
                echo '<hr>';
            }
			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'shapely' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->
    <?php if (is_single()): ?>
        <footer class="entry-footer">
            <?php shapely_entry_footer(); ?>
        </footer><!-- .entry-footer -->
    <?php endif; ?>
	
</article><!-- #post-## -->
