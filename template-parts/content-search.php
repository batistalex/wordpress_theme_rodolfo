<?php
/**
 * Template part for displaying results in search pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Shapely
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php
        if(has_post_thumbnail()){
        ?>
         <a class="text-center" href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php
            the_post_thumbnail('shapely-featured',array( 'class' => 'mb24')); ?>
        </a><?php
        }
        ?><div class="title-line">
        <a class="hidden-xs hidden-sm" href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ), get_the_author_meta( 'user_nicename' ) ); ?>"><div class="user-avatar"><?php echo get_avatar( get_the_author_meta( 'ID' ), 100 ); ?></div></a>
        <div class="title-content">
        <?php
        the_title( '<h2 class="post-title entry-title hidden-xs hidden-sm"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>');
         the_title( '<h4 class="post-title entry-title visible-sm-block"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h4>');
            the_title( '<h6 class="post-title entry-title visible-xs-block"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h6>');
        shapely_posted_on();
        ?></div>/</div>
		
	</header>
<!-- 
	<footer class="entry-footer">
		 shapely_entry_footer();
	</footer> --><!-- .entry-footer -->
</article><!-- #post-## -->

