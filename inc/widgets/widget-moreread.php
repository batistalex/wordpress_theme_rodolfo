<?php

/**
 * Shapely Moreread Posts Widget
 * Shapely Theme
 */
class shapely_moreread_posts extends WP_Widget {

  function __construct() {

    $widget_ops = array('classname' => 'shapely-moreread-posts', 'description' => esc_html__("Widget to show the most read posts with thumbnails", 'shapely'));
    parent::__construct('shapely_moreread_posts', esc_html__('[Shapely] More read Posts', 'shapely'), $widget_ops);
  }

  function widget($args, $instance) {
    extract($args);
    $title = isset($instance['title']) ? $instance['title'] : esc_html__('moreread Posts', 'shapely');
    $limit = isset($instance['limit']) ? $instance['limit'] : 5;

    echo $before_widget;
    echo $before_title;
    echo $title;
    echo '<hr>';
    echo $after_title;

    /**
     * Widget Content
     */
    ?>

    <!-- moreread posts -->
    <!--http://wordpress.stackexchange.com/questions/145642/sorting-posts-according-to-view-counts-not-working -->
    <div class="moreread-posts-wrapper nolist">

      <?php
      $featured_args = array(
       'posts_per_page'  => $limit,  /* get 4 posts, or set -1 for all */
       'orderby'      => 'meta_value_num',  /* this will look at the meta_key you set below */
       'meta_key'     => 'post_views_count',
       'order'        => 'DESC',
       'post_status'  => 'publish'
      );

      $featured_query = new WP_Query($featured_args);

      if ($featured_query->have_posts()) : ?>

        <div class="link-list moreread-posts row"><?php

          while ($featured_query->have_posts()) : $featured_query->the_post(); 

            $fname = get_the_author_meta('first_name');
            $lname = get_the_author_meta('last_name');
            $full_name = "{$fname} {$lname}";
           if (get_the_content() != '' && has_post_thumbnail()) : ?>

              <!-- content -->
              <div class="post-content col-xs-12 col-sm-6 col-md-4 border-b-green">
                <a href="<?php echo get_permalink(); ?>">
                <footer>
                  <div class="thumb-content">
                    <?php the_post_thumbnail('shapely-featured-widget-posts'); ?>
                  </div>
                  <h4><?php the_title(); ?></h4>
                  <span><?php  printf(__('By %1$s','shapely'),$full_name); ?></span>
                   <ul class="post-meta" style="font-size: 10px; display: block; margin-top: -10px;"><li style=" text-align: center; float: none;"><?php echo getPostViews(get_the_ID());?></li></ul>
                </footer>
                </a>
                <!-- echo getPostViews(get_the_ID()); -->
              </div>
              <!-- end content -->

            <?php endif; ?>

          <?php endwhile; ?>
        </div><?php

      endif;
      wp_reset_postdata();
      ?>

    </div> <!-- end posts wrapper -->
    <?php
    echo $after_widget;
  }

  function form($instance) {

    if (!isset($instance['title']))
      $instance['title'] = esc_html__('moreread Posts', 'shapely');
    if (!isset($instance['limit']))
      $instance['limit'] = 5;
    ?>

    <p><label for="<?php echo $this->get_field_id('title'); ?>"><?php esc_html_e('Title', 'shapely') ?></label>

      <input  type="text" value="<?php echo esc_attr($instance['title']); ?>"
              name="<?php echo $this->get_field_name('title'); ?>"
              id="<?php $this->get_field_id('title'); ?>"
              class="widefat" />
    </p>

    <p><label for="<?php echo $this->get_field_id('limit'); ?>"><?php esc_html_e('Limit Posts Number', 'shapely') ?></label>

      <input  type="text" value="<?php echo esc_attr($instance['limit']); ?>"
              name="<?php echo $this->get_field_name('limit'); ?>"
              id="<?php $this->get_field_id('limit'); ?>"
              class="widefat" />
    <p>

    <?php
  }

  /**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? esc_html( $new_instance['title'] ) : '';
		$instance['limit'] = ( ! empty( $new_instance['limit'] ) && is_numeric( $new_instance['limit'] )  ) ? esc_html( $new_instance['limit'] ) : '';

		return $instance;
	}

}
?>
