(function($) {
  //loop solution http://stackoverflow.com/questions/15628500/how-do-i-loop-next?answertab=active#tab-top
  $.animateText = {
    start: function(time){
      $("h1.content__heading.active").animate({opacity: 1}, 1000);
      setInterval(function(){
        $next = $("h1.content__heading.active").removeClass("active").css('opacity',0).next("h1.content__heading");
        if($next.length){
          $next.addClass("active");
        }else{
          $("h1.content__heading:first").addClass("active");
        }
        $("h1.content__heading.active").animate({opacity: 1}, 1000);
      },time);
    },
    stop: function() {

    }
  }

})(jQuery)


jQuery("document").ready(function($){
    var nav = $('nav');
    $(window).scroll(function () {
        if ($(this).scrollTop() > 450) {
            nav.addClass("scroll-nav");
        } else {
            nav.removeClass("scroll-nav");
        }
    });
    //function called here
    $.animateText.start(5000);


	  var $obj = $('div.quotes');
    $(window).scroll(function() {
		var yPos = -($(window).scrollTop() / $obj.data('speed'));
		var bgpos = '50% '+ yPos + 'px';
		$obj.css('background-position', bgpos );
	});



});
