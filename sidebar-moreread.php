<?php
/**
 * The Sidebar widget area for more read posts.
 *
 * @package shapely
 */


	if ( ! is_active_sidebar( 'moreread' ) )
		return;
	// If we made it this far we must have widgets.
?>

	<div class="moreread-widget-area">
		<div class="col-md-12 col-sm-12 moreread-widget" role="complementary">
			<?php dynamic_sidebar( 'moreread' ); ?>
		</div><!-- .widget-area .first -->
	</div>