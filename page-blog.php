<?php

get_header(); ?>
    <?php 
    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
    $args = array(
      'post_type'      => 'post',
      'posts_per_page' => 6,
      'paged'          => $paged
    );
    $my_query = new WP_Query( $args ); 
    
    ?>
    <?php $layout_class = ( function_exists('shapely_get_layout_class') ) ? shapely_get_layout_class(): ''; ?>  
    <div id="primary" class="col-md-8 mb-xs-24 <?php echo $layout_class; ?>"><?php
        if ( $my_query->have_posts() ) : 
         
            /* Start the Loop */
            while ( $my_query->have_posts() ) : $my_query->the_post();

                /*
                 * Include the Post-Format-specific template for the content.
                 * If you want to override this in a child theme, then include a file
                 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
                 */
             
                get_template_part( 'template-parts/content', get_post_format() );
            
            endwhile;
            
            if (function_exists("shapely_pagination")):
                shapely_pagination($my_query->max_num_pages,3,$paged);
            endif;
            ?>

            <?php
            wp_reset_postdata();
        else :

            get_template_part( 'template-parts/content', 'none' );

        endif; ?>        
    </div><!-- #primary -->
<?php
get_sidebar();
get_sidebar( 'moreread' );
get_footer();
